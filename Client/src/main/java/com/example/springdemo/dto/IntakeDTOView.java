package com.example.springdemo.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class IntakeDTOView implements Serializable {

    private Integer idintake;
    private Integer period;
    private LocalDate start_time;
    private List<String > intake_intervals;
    private Integer idPatient;
    private List<String > drugDTOs;

    public IntakeDTOView() {
    }

    public IntakeDTOView(Integer idintake, Integer period, LocalDate start_time, List<String> intake_intervals, Integer idPatient, List<String> drugDTOs) {
        this.idintake = idintake;
        this.period = period;
        this.start_time = start_time;
        this.intake_intervals = intake_intervals;
        this.idPatient = idPatient;
        this.drugDTOs = drugDTOs;
    }

    public LocalDate getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDate start_time) {
        this.start_time = start_time;
    }

    public Integer getIdintake() {
        return idintake;
    }

    public void setIdintake(Integer idintake) {
        this.idintake = idintake;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public List<String> getIntake_intervals() {
        return intake_intervals;
    }

    public void setIntake_intervals(List<String > intake_intervals) {
        this.intake_intervals = intake_intervals;
    }

    public Integer getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Integer idPatient) {
        this.idPatient = idPatient;
    }

    public List<String> getDrugDTOs() {
        return drugDTOs;
    }

    public void setDrugDTOs(List<String> drugDTOs) {
        this.drugDTOs = drugDTOs;
    }
}