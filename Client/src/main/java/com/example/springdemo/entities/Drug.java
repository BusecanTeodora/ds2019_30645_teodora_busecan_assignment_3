package com.example.springdemo.entities;

import java.io.Serializable;

public class Drug implements Serializable {

    private Integer iddrug;


    private String name;

    private String sideeffect;

    private Integer dosage;


    public Drug() {
    }

    public Drug(Integer iddrug, String name, String sideeffect, Integer dosage) {
        this.iddrug = iddrug;
        this.name = name;
        this.sideeffect = sideeffect;
        this.dosage = dosage;
    }

    public Integer getIddrug() {
        return iddrug;
    }

    public void setIddrug(Integer iddrug) {
        this.iddrug = iddrug;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideeffect() {
        return sideeffect;
    }

    public void setSideeffect(String sideeffect) {
        this.sideeffect = sideeffect;
    }

    public Integer getDosage() {
        return dosage;
    }

    public void setDosage(Integer dosage) {
        this.dosage = dosage;
    }
}
