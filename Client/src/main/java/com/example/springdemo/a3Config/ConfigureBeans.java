package com.example.springdemo.a3Config;

import com.example.springdemo.services.A3Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

@Configuration
public class ConfigureBeans {

    @Bean
    public RmiProxyFactoryBean getClientService() {
        RmiProxyFactoryBean rmiProxyFactory = new RmiProxyFactoryBean();
        rmiProxyFactory.setServiceUrl("rmi://localhost:1099/A3Service");
        rmiProxyFactory.setServiceInterface(A3Service.class);
        return rmiProxyFactory;
    }

}
