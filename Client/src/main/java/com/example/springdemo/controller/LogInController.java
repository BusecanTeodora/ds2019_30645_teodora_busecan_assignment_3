package com.example.springdemo.controller;

import com.example.springdemo.Client;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Controller;

@Controller
public class LogInController  {


    @FXML
    TextField logInUsername;

    @FXML
    PasswordField logInPassword;
//
//  private A3Service a3Service ;

    @Autowired
    private ConfigurableApplicationContext context;
    private Parent secondView;

    public  static Integer iduser;


    public LogInController() {

    }

    public void logIn() {
        Integer loginId = Client.service.isLoggedIn(logInUsername.getText(), logInPassword.getText());
        System.out.println(loginId);
        System.out.println(Client.service.getIntakes(19));
        try {
            if(loginId != -1) {
              iduser = loginId;
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/a3.fxml"));
                loader.setControllerFactory(context::getBean);
                secondView = loader.load();
                Scene newScene = new Scene(secondView);
                Stage stage = (Stage) logInUsername.getScene().getWindow();
                stage.setScene(newScene);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
