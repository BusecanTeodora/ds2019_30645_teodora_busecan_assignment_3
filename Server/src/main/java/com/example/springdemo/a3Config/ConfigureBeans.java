package com.example.springdemo.a3Config;

import com.example.springdemo.services.A3Service;
import com.example.springdemo.services.A3ServiceImplement;
import org.springframework.context.annotation.Bean;

public class ConfigureBeans {

    @Bean
    A3Service getString() {
        return new A3ServiceImplement();
    }
}
