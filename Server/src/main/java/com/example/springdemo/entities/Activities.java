package com.example.springdemo.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "activities")
public class Activities {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idactivities", unique = true, nullable = false)
    private Integer idactivities;

    @ManyToOne
    @JoinColumn(name = "idpatient")
    private Patient patient;


    @Column(name = "start_time")
    private Timestamp start_time;

    @Column(name = "end_time")
    private Timestamp end_time;

    @Column(name = "activity")
    private String activity;

    public Activities(Integer idactivities, Patient patient, Timestamp start_time, Timestamp end_time, String activity) {
        this.idactivities = idactivities;
        this.patient = patient;
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity = activity;
    }

    public Activities() {
    }

    public Integer getIdactivities() {
        return idactivities;
    }

    public void setIdactivities(Integer idactivities) {
        this.idactivities = idactivities;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Timestamp getStart_time() {
        return start_time;
    }

    public void setStart_time(Timestamp start_time) {
        this.start_time = start_time;
    }

    public Timestamp getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Timestamp end_time) {
        this.end_time = end_time;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}