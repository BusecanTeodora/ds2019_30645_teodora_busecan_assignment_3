package com.example.springdemo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class DrugStatus implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String message;
    private long patientId;

    public DrugStatus(String message, long patientId) {
        this.message = message;
        this.patientId = patientId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }
}
