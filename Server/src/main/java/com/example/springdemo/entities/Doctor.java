package com.example.springdemo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "doctor")
public class Doctor implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "iddoctor", unique = true, nullable = false)
    private Integer iddoctor;

    @ManyToOne
    @JoinColumn(name = "iduser")
    private User user;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "doctor", cascade = CascadeType.ALL)
    private Set<Caregiver> caregiverSet;

    public Doctor() {
    }

    public Doctor(Integer iddoctor, String name) {
        this.iddoctor = iddoctor;
        this.name = name;
    }


    public Integer getIddoctor() {
        return iddoctor;
    }

    public void setIddoctor(Integer iddoctor) {
        this.iddoctor = iddoctor;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
