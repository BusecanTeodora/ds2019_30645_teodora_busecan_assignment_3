package com.example.springdemo.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "intake_drugs")
public class IntakeDrugs implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "idintake_drugs", unique = true, nullable = false)
    private Integer idintake_drugs;

    @ManyToOne
    @JoinColumn(name = "iddrug")
    private Drug drug;

    @Column(name = "intake_intervals")
    private String intake_intervals;

    @ManyToOne
    @JoinColumn(name = "idintake")
    private Intake idintake;


    public IntakeDrugs() {
    }

    public IntakeDrugs(Integer idintake_drugs, Drug drug, String intake_intervals, Intake intake) {
        this.idintake_drugs = idintake_drugs;
        this.drug = drug;
        this.intake_intervals = intake_intervals;
        this.idintake = intake;
    }

    public Integer getIdintake_drugs() {
        return idintake_drugs;
    }

    public void setIdintake_drugs(Integer idintake_drugs) {
        this.idintake_drugs = idintake_drugs;
    }

    public Drug getDrug() {
        return drug;
    }

    public void setDrug(Drug drug) {
        this.drug = drug;
    }

    public String getIntake_intervals() {
        return intake_intervals;
    }

    public void setIntake_intervals(String intake_intervals) {
        this.intake_intervals = intake_intervals;
    }

    public Intake getIntake() {
        return idintake;
    }

    public void setIntake(Intake intake) {
        this.idintake = intake;
    }
}
