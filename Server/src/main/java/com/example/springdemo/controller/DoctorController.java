package com.example.springdemo.controller;

import com.example.springdemo.dto.DoctorDTO;
import com.example.springdemo.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @PostMapping(value = "/getDoctor")
    public DoctorDTO getDoctor(@RequestBody String username){
        return doctorService.findDoctorByUsername(username);
    }


}
