package com.example.springdemo.controller;


import com.example.springdemo.dto.PatientDTOinsert;
import com.example.springdemo.dto.UserDTOView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.springdemo.dto.PatientDTO;
import com.example.springdemo.services.PatientService;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping(value = "/{idpatient}")
    public PatientDTO findById(@PathVariable("idpatient") Integer id){
        return patientService.findpatientById(id);
    }

    @GetMapping()
    public List<PatientDTOinsert> findAll(){
        return patientService.findAll();
    }

    @PostMapping()
    public Integer insertPatientDTO(@RequestBody PatientDTOinsert patientDTOinsert){
        return patientService.insert(patientDTOinsert);
    }

    @PostMapping("/modify")
    public Integer updatePatient(@RequestBody PatientDTOinsert patientDTOinsert) {
        return patientService.update(patientDTOinsert);
    }

    @PostMapping(value = "/delete")
    public void deletePatient(@RequestBody PatientDTOinsert patientDTOinsert){ patientService.delete(patientDTOinsert);}

    @PostMapping(value = "/getPatient")
    public PatientDTOinsert getPatient(@RequestBody String username){
        return patientService.findPatientByUsername(username);
    }
}
