package com.example.springdemo.repositories;

import com.example.springdemo.entities.Activities;
import com.example.springdemo.entities.Caregiver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ActivitiesRepository extends JpaRepository<Activities, Integer> {

}
