package com.example.springdemo.repositories;

import com.example.springdemo.entities.DrugStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrugStatusRepository extends JpaRepository<DrugStatus, Long> {
}
