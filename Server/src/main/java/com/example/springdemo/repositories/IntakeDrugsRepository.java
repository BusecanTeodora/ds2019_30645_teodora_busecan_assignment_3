package com.example.springdemo.repositories;

import com.example.springdemo.entities.IntakeDrugs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntakeDrugsRepository extends JpaRepository<IntakeDrugs, Integer> {
}
