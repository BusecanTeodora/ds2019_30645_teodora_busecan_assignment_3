package com.example.springdemo.dto;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

public class ActivitiesDTO {
  private Integer idactivities;

    private Integer patient;


    private Timestamp start_time;

    private Timestamp end_time;

    private String activity;

    public ActivitiesDTO(Integer idactivities, Integer patient, Timestamp start_time, Timestamp end_time, String activity) {
        this.idactivities = idactivities;
        this.patient = patient;
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity = activity;
    }

    public ActivitiesDTO(Integer patient, Timestamp start_time, Timestamp end_time, String activity) {
        this.patient = patient;
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity = activity;
    }

    public ActivitiesDTO() {
    }

    public Integer getIdactivities() {
        return idactivities;
    }

    public void setIdactivities(Integer idactivities) {
        this.idactivities = idactivities;
    }

    public Integer getPatient() {
        return patient;
    }

    public void setPatient(Integer patient) {
        this.patient = patient;
    }

    public Timestamp getStart_time() {
        return start_time;
    }

    public void setStart_time(Timestamp start_time) {
        this.start_time = start_time;
    }

    public Timestamp getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Timestamp end_time) {
        this.end_time = end_time;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}