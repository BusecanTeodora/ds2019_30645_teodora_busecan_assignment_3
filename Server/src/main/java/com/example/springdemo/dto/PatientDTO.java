package com.example.springdemo.dto;

import java.sql.Date;
import java.util.Objects;

public class PatientDTO {

    private Integer idpatient;
    private String name;
    private String birthday;
    private String address;
    private String gender;

    private CaregiverDTO caregiverDTO;
    private UserDTO userDTO;

    public PatientDTO() {
    }

    public PatientDTO(Integer idpatient, String name, String birthday, String address, String gender, CaregiverDTO caregiverDTO, UserDTO userDTO) {
        this.idpatient = idpatient;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.gender = gender;
        this.caregiverDTO = caregiverDTO;
        this.userDTO = userDTO;
    }

    public Integer getIdpatient() {
        return idpatient;
    }

    public void setIdpatient(Integer idpatient) {
        this.idpatient = idpatient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public CaregiverDTO getCaregiverDTO() {
        return caregiverDTO;
    }

    public void setCaregiverDTO(CaregiverDTO caregiverDTO) {
        this.caregiverDTO = caregiverDTO;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO that = (PatientDTO) o;
        return Objects.equals(idpatient, that.idpatient) &&
                Objects.equals(name, that.name) &&
                Objects.equals(birthday, that.birthday) &&
                Objects.equals(address, that.address) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(caregiverDTO, that.caregiverDTO) &&
                Objects.equals(userDTO, that.userDTO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idpatient, name, birthday, address, gender, caregiverDTO, userDTO);
    }
}
