package com.example.springdemo.dto;

import java.util.Objects;

public class UserDTOView {

    private Integer iduser;
    private String username;
    private String password;
    private Integer usertype;

    public UserDTOView() {
    }

    public UserDTOView(Integer iduser, String username, String password, Integer usertype) {
        this.iduser = iduser;
        this.username = username;
        this.password = password;
        this.usertype = usertype;
    }

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getUserTypeDTO() {
        return usertype;
    }

    public void setUserTypeDTO(Integer usertype) {
        this.usertype = usertype;
    }

}