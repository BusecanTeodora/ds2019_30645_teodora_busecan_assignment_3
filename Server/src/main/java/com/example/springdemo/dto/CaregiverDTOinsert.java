package com.example.springdemo.dto;

import com.example.springdemo.entities.Intake;

import java.sql.Date;
import java.util.Objects;

public class CaregiverDTOinsert {

    private Integer idcaregiver;
    private String name;
    private String birthday;
    private String address;
    private String gender;
    private Integer iddoctor;
    private Integer iduser;

    public CaregiverDTOinsert(Integer idcaregiver, String name, String birthday, String address, String gender, Integer iddoctor, Integer iduser, String username, String password) {
        this.idcaregiver = idcaregiver;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.gender = gender;
        this.iddoctor = iddoctor;
        this.iduser = iduser;
        this.username = username;
        this.password = password;
    }

    private String username;
    private String password;

    public CaregiverDTOinsert() {
    }

    public CaregiverDTOinsert(Integer idcaregiver, String name, String  birthday, String address, String gender, Integer idDoctor, Integer idUser) {
        this.idcaregiver = idcaregiver;
        this.name = name;
        this.birthday = birthday;
        this.address = address;
        this.gender = gender;
        this.iddoctor = idDoctor;
        this.iduser = idUser;
    }

    public Integer getIdcaregiver() {
        return idcaregiver;
    }

    public void setIdcaregiver(Integer idcaregiver) {
        this.idcaregiver = idcaregiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getIdDoctor() {
        return iddoctor;
    }

    public void setIdDoctor(Integer idDoctor) {
        this.iddoctor = idDoctor;
    }

    public Integer getIdUser() {
        return iduser;
    }

    public void setIdUser(Integer idUser) {
        this.iduser = idUser;
    }

    public Integer getIddoctor() {
        return iddoctor;
    }

    public void setIddoctor(Integer iddoctor) {
        this.iddoctor = iddoctor;
    }

    public Integer getIduser() {
        return iduser;
    }

    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}