package com.example.springdemo.services;


import com.example.springdemo.dto.IntakeDTOView;
import com.example.springdemo.entities.*;
import com.example.springdemo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class A3ServiceImplement implements A3Service {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private IntakeRepository intakeRepository;
    @Autowired
    private DrugStatusRepository drugStatusRepository;

    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private IntakeDrugsRepository intakeDrugsRepository;
    @Autowired
    private DrugRepository drugRepository;


    @Override
    public String getString(String pickUpLocation) {
        return pickUpLocation + " from server";
    }


    @Override
    public Integer isLoggedIn(String username, String password) {
        User user = userRepository.findByUsername(username);
        if(user != null && user.getPassword().contentEquals(password)) {
            if ( user.getUserType().getUserType() == 3) {
                return user.getIduser();
            }
            return -1;
        }
        return -1;
    }

    @Override
    public void intakeLog(String log, Integer patientId) {
        DrugStatus drugStatus = new DrugStatus(log, patientId);
        drugStatusRepository.save(drugStatus);
    }

    @Override
    public Drug getDrug(String name) {
        return drugRepository.findByName(name);
    }

    @Override
    public List<IntakeDTOView> getIntakes(Integer idUSer) {
        User user = userRepository.getOne(idUSer);
       List<Patient> patients = patientRepository.findAll();
       Integer idpatient = -1;
       for(Patient p: patients){
           if(p.getUser().getIduser().equals(idUSer)){
               idpatient = p.getIdpatient();
           }
       }
        List<Intake> intakes = intakeRepository.findAll();
        List<IntakeDrugs> intakeDrugs = intakeDrugsRepository.findAll();

        List<IntakeDTOView> intakeDTOViews = new ArrayList<>();
        Integer idintake = null;

        for(Intake i : intakes) {
            IntakeDTOView intakeDTOView = new IntakeDTOView();
            List<String> intake_intervals = new ArrayList<>();
            List<String> drugs = new ArrayList<>();
            if (i.getPatient().getIdpatient() == idpatient) {

                for (IntakeDrugs intakeDrugs1 : intakeDrugs) {
                    idintake = i.getIdintake();
                    if (intakeDrugs1.getIntake().getIdintake() == idintake) {
                        intakeDTOView.setPeriod(i.getPeriod());
                        intakeDTOView.setIdPatient(idpatient);
                        intakeDTOView.setIdintake(i.getIdintake());
                        intakeDTOView.setStart_time(i.getStart_time());
                        intake_intervals.add(intakeDrugs1.getIntake_intervals());
                        drugs.add(intakeDrugs1.getDrug().getName());
                    }
                }
            }
            intakeDTOView.setIntake_intervals(intake_intervals);
            intakeDTOView.setDrugDTOs(drugs);
            if(intakeDTOView.getIdintake()!= null)
                intakeDTOViews.add(intakeDTOView);
        }
        List<IntakeDTOView> finalList = new ArrayList<>();
        for(IntakeDTOView intakeDTOView:intakeDTOViews){
            if(intakeDTOView.getStart_time().plusDays(intakeDTOView.getPeriod()).isAfter(LocalDate.now()) && (intakeDTOView.getStart_time().isBefore(LocalDate.now()) || intakeDTOView.getStart_time().isEqual(LocalDate.now()))){
                finalList.add(intakeDTOView);
            }
        }
        return finalList;

    }
}