package com.example.springdemo.services;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverDTOinsert;
import com.example.springdemo.dto.PatientDTOinsert;
import com.example.springdemo.dto.builders.CaregiverBuilder;
import com.example.springdemo.entities.*;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CaregiverService {


    private final CaregiverRepository caregiverRepository;
    private final DoctorRepository doctorRepository;
    private final PatientRepository patientRepository;
    private final UserRepository userRepository;
    private final IntakeRepository intakeRepository;
    private UserService userService;
    private PatientService patientService;


    @Autowired
    public CaregiverService(IntakeRepository intakeRepository,CaregiverRepository caregiverRepository,UserService userService,DoctorRepository doctorRepository,UserRepository userRepository,PatientRepository patientRepository, PatientService patientService) {
        this.caregiverRepository = caregiverRepository;
        this.intakeRepository = intakeRepository;
        this.userService = userService;
        this.doctorRepository = doctorRepository;
        this.userRepository  = userRepository;
        this.patientRepository = patientRepository;
        this.patientService = patientService;
    }

    public CaregiverDTO findcaregiverById(Integer id){
        Optional<Caregiver> caregiver  = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "user id", id);
        }
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverDTOinsert> findAll(){
        List<Caregiver> caregivers = caregiverRepository.findAll();
        List<CaregiverDTOinsert> caregiverDTOinserts = new ArrayList<>();
        for(Caregiver c: caregivers){
            CaregiverDTOinsert caregiverDTOinsert= new CaregiverDTOinsert(c.getIdcaregiver(),c.getName(),c.getDate(),c.getAddress(),c.getGender(),c.getDoctor().getIddoctor(),c.getUser().getIduser());
            caregiverDTOinserts.add(caregiverDTOinsert);
        }

        return caregiverDTOinserts;
    }

    public Integer insert(CaregiverDTOinsert caregiverDTOinsert) {
        Caregiver caregiver = new Caregiver();
        List<Caregiver> caregivers = caregiverRepository.findAll();
        for(Caregiver c: caregivers){
            if(c.getUser().getIduser() == caregiverDTOinsert.getIdUser()){
                return null;
            }
        }
        UserType userType = new UserType();
        userType.setUserType(2);
        User user = new User();
        user.setUsername(caregiverDTOinsert.getUsername());
        user.setPassword(caregiverDTOinsert.getPassword());
        user.setUserType(userType);

        Integer id = (userRepository.save(user)).getIduser();

        Doctor doctor = doctorRepository.getOne(caregiverDTOinsert.getIdDoctor());
        caregiver.setIdcaregiver(caregiverDTOinsert.getIdcaregiver());
        caregiver.setAddress(caregiverDTOinsert.getAddress());
        caregiver.setDoctor(doctor);
        caregiver.setGender(caregiverDTOinsert.getGender());
        caregiver.setDate(caregiverDTOinsert.getBirthday());
        caregiver.setUser(user);
        caregiver.setName(caregiverDTOinsert.getName());
        return caregiverRepository.save(caregiver).getIdcaregiver();
    }

    public Integer update(CaregiverDTOinsert caregiverDTOinsert) {
        Caregiver caregiver = new Caregiver();
        User user = userRepository.getOne(caregiverDTOinsert.getIdUser());
        Doctor doctor = doctorRepository.getOne(caregiverDTOinsert.getIdDoctor());
        caregiver.setIdcaregiver(caregiverDTOinsert.getIdcaregiver());
        caregiver.setAddress(caregiverDTOinsert.getAddress());
        caregiver.setDoctor(doctor);
        caregiver.setGender(caregiverDTOinsert.getGender());
        caregiver.setDate(caregiverDTOinsert.getBirthday());
        caregiver.setUser(user);
        caregiver.setName(caregiverDTOinsert.getName());
        return caregiverRepository.save(caregiver).getIdcaregiver();
    }

    public void delete(CaregiverDTOinsert caregiverDTOinsert){
        List<Intake> intakes = intakeRepository.findAll();
        List<Patient> patients = patientRepository.findAll();

        for(Patient p:patients) {
            if (p.getCaregiver().getIdcaregiver() == caregiverDTOinsert.getIdcaregiver()) {
                for(Intake intake: intakes){
                    if(intake.getPatient().getIdpatient()== p.getIdpatient()) {
                        intakeRepository.delete(intake);
                    }
                }
                patientRepository.deleteById(p.getIdpatient());
            }
        }
        this.caregiverRepository.deleteById(caregiverDTOinsert.getIdcaregiver());
    }


    public CaregiverDTOinsert findCaregiverByUsername(String username){
        Integer idUser = userService.findUserByUsername(username);
        List<Caregiver> caregivers = caregiverRepository.findAll();
        CaregiverDTOinsert caregiverDTOinsert = new CaregiverDTOinsert();
        for(Caregiver c:caregivers){
            if(c.getUser().getIduser() == idUser){
                caregiverDTOinsert= new CaregiverDTOinsert(c.getIdcaregiver(),c.getName(),c.getDate(),c.getAddress(),c.getGender(),c.getDoctor().getIddoctor(),c.getUser().getIduser());

            }
        }
        return caregiverDTOinsert;
    }

    public List<PatientDTOinsert> seeAllPatients(Integer idCaregiver) {
        List<Patient> patients = patientRepository.findAll();
        List<PatientDTOinsert> patientDTOinserts = new ArrayList<>();
        for(Patient p : patients){
            if(p.getCaregiver().getIdcaregiver() == idCaregiver) {
                PatientDTOinsert patientDTOinsert1 = new PatientDTOinsert(p.getIdpatient(),p.getName(),p.getDate(),p.getAddress(),p.getGender(),p.getCaregiver().getIdcaregiver(),p.getUser().getIduser());
                patientDTOinserts.add(patientDTOinsert1);
            }
        }
        return  patientDTOinserts;
    }
}
